$( document ).ready(function() {
	
	//Zobrazeni formu pro vlozeni komentaru
	$('a.add_comment').click(function(e) {
		e.preventDefault();
		
		$('form.form_comments').slideToggle();
	});
	//Zobrazeni formu pro vlozeni komentaru
    
        
    //START CAROUSEL
    $(".carousel").carousel({
        vertical: true
    });
    //START CAROUSEL
    
    
    //CAROUSEL SHOW IMAGES
    $('.carousel a.show_img').click(function(e) {
	    e.preventDefault();
	    
	    $('.detail_images .main_image img').attr('src', $(this).attr('href'));
	    $('.detail_images .main_image img').data('index', $(this).parent().index());
	    
    })
    //CAROUSEL SHOW IMAGES
    
    
    //MAIN IMAGE ROTATION
    $('.main_img_rotation').click(function(e) {
	    e.preventDefault();
	    
	    var $thumbs = $('.carousel ul li').size();
	    var $img = parseInt($(this).children('img').data('index'));
	    var $next = 0;
	    
	    if(($img + 1) == $thumbs) {
		    $next = 0;
	    
	    } else {
			$next = $img + 1;    
	    }
	    
	    $(this).children('img').data('index', $next);
	    $(this).children('img').attr('src', $('.carousel ul li:eq('+$next+') a').attr('href'));
    })
    //MAIN IMAGE ROTATION
    
    
    //Nastaveni vysky textarea podle textu
    if($('#link_box').size()) {
    	$('#link_box').height( $('#link_box')[0].scrollHeight );
    }
    //Nastaveni vysky textarea podle textu
    
    //Copy URL
    $('#link_box').click(function() {
		$(this).select();    
    });
	//Copy URL
});