/**
 * @author JAN VOJACEK
 */
var filesDataFirst = true;
var filesData;
var filesDataUp = new Array();
var fileNumber = 0;
var fileUploadedCount = 0;
var positionKB;
var totalKB;
var speedKB = new Array();
var time = 0;
var qwe;
var allFilesKB = 0;
var allDoneKB;
var allFirst = true;
var intervalOne = true;
var fileMaxSize = 2; //GB
var filesError = new Array();
var eroticContent = false;

function pageLoaded(){
	document.getElementById("start_upload").addEventListener("click",uploadAccept,false);
	document.getElementById("files_upload").addEventListener("change",addingFiles,false);
	//document.getElementById("cancel_upload").addEventListener("click",uploadCancel,false);
	//document.getElementById("showDetails").addEventListener("click",toggleMore,false);
	document.getElementById("new_upload").addEventListener("click",newUp,false);
}
function startUploading(){
	var formData = new FormData();
	formData.append("files",filesData.item(fileNumber));
	formData.append("erotic_content", eroticContent);
	//console.log(formData);
	
	if(filesDataUp[fileNumber]==false){
		fileNumber++;
		startUploading();
	}else{
	//zobrazení názvu souboru a kolik z kolika do nadpisu
	var tempName = filesData.item(fileNumber).name;
	var tempNumber = fileNumber + 1;
	$("#upLessTitle").html(tempName + " (" + tempNumber + " z " + fileUploadedCount + ")");
	$("#L01").html(fileNumber + " z " + fileUploadedCount);
	if(intervalOne==true){
	intervalOne = false;
	qwe = window.setInterval(uploadSpeed,1000);/* NASTAVENI POCITANI RYCHLOSTI UPLOADU ZA SEKUNDU KAZDOU SEKUNDU */
	}
	speedKB.length = 0;
	//odeslání souboru (formData) pomocí AJAX (jQuery)
	var jQueryAjaxObject = $.ajax({
		url: "uploadData.php",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		xhr: function(){ /*začátek xhr*/
			var xhrObject = $.ajaxSettings.xhr();
			if(xhrObject.upload){
				xhrObject.upload.addEventListener("progress",function(event){
					var percentage = 0;var position = event.loaded || event.position;var total = event.total;
					console.log(position + " - " + total);
					if (event.lengthComputable){
						percentage = Math.ceil(position / total * 100);console.log("PROCENTA: " + percentage);
						$(".progress_bar .bar").attr('style', 'width:'+percentage+'%');$("#percentProgress").html(percentage + "%");
						positionKB = Math.round(position / 1000);totalKB = Math.round(total / 1000);
						$("#L02").html(positionKB + " / " + totalKB + " KB");
					}
				},false);
			}
			return xhrObject;
		}/*konec xhr*/
	}).success(function(echoData){
		time = 0;
		//console.log(echoData);
		if(fileNumber < filesData.length-1){
			fileNumber++;
			startUploading();
		}else{
			//TADY SE UKONCI NAHRAVANI :)
			$("#upload_step2").hide();
			//$("#uploadDetailsDiv").css("display","none");
			//$("#mainUploadTitle").html("NÁSLEDUJÍCÍ (" + fileUploadedCount + ") SOUBORY BYLY ÚSPĚŠNĚ NAHRÁNY:");
			//$("#newUpload").css("display","block");
			
			$("#upload_step3 .upload_summary tbody").empty();var count = 1;
			for(var p = 0;p<filesData.length;p++){
				if(filesDataUp[p]==true){
				$("#upload_step3 .upload_summary tbody").append(
				"<tr>" + 
				"<td class=\"col1\">" + filesData.item(p).name + "</td>" +
				"<td class=\"col2\">" + sizeToMB(filesData.item(p).size) + " MB</td>" +
				"<td class=\"col3\">" + 
					"<a href=\"#\" class=\"share_twitter\" title=\"Twitter\"></a>" + 
					"<a href=\"#\" class=\"share_gplus\" title=\"Google+\"></a>" + 
					"<a href=\"#\" class=\"share_facebook\" title=\"Facebook\"></a>" + 
				"</td>" +
				"</tr>" +
				"<tr class=\"file_url\">" + 
				"<td colspan=\"3\">" +
					"<div class=\"copy_url_cover\">" +
						"<input type=\"text\" name=\"file"+count+"\" id=\"file"+count+"\" onclick=\"copyUrlInput(this);\" readonly value=\"" + filesData.item(p).name + "\" />" + 
						"<a href=\"#\" class=\"copy_url button_red_small\" onclick=\"copyUrl(this);return false;\" title=\"Kopírovat URL\">Kopírovat URL</a>" + 
						"<div class=\"clear\"></div>" + 
					"</div>" + 
					"<a href=\"#\" class=\"delete_file\" title=\"smazat soubor\">smazat soubor</a>" + 
				"</td>" +
				"</tr>");
				
				
				count++;
				}
			}
			
			$("#upload_step3").show();
		}
	});
	}
}
function addingFiles(event){
	if(filesDataFirst==false){
		$(".upload_file_list").empty();
	}
	filesData = event.target.files;
	filesDataFirst = false;
	//console.log(filesData);console.log(filesData.item(fileNumber));
	//console.log("Délka filesData je: " + filesData.length);
	
	//console.log(filesData.length);
	var errorSize = false;
	for(var q = 0;q<filesData.length;q++){
		if(sizeToGB(filesData.item(q).size) > fileMaxSize) {
			errorSize = true;
			
		} else {
			var number = q + 1;
			$("#upload_step1 .upload_file_list").append("<tr><th>" + filesData.item(q).name + 
			"</th><td>" + sizeToMB(filesData.item(q).size) + " MB</td><td><a href=\"#\" class=\"icon_delete\" title=\"Odstranit z fronty\" onclick=\"removeFile(this);return false;\" id=\"" + q + "\"></td></tr>");
			var idD = $("#" + q).attr("id");
			var idB = parseInt(idD);
			//console.log("ID JE: "+ idB + " Hodnota filesData je: " + filesData.item(idB).name);
			filesDataUp[q] = true;
			//console.log(filesDataUp[q]);
		}
		
	}
	fileUploadedCount = filesData.length;
	
	if(errorSize) alert('Některé vámi vybrané soubory překračují max. povolenou velikost ' + fileMaxSize + ' GB.');
	
}
function removeFile(element){
	var id = $(element).attr("id");//console.log("NEKDO KLIKL NA: " + id);
	var idS = parseInt(id);
	fileUploadedCount = 0;
	filesDataUp[idS] = false;for(var q = 0;q<filesDataUp.length;q++){/*console.log(filesDataUp[q]);*/if(filesDataUp[q]==true){fileUploadedCount++;}}
	
	$(element).closest('.upload_file_list tr').fadeOut();
}
function uploadAccept(){
	//Eroticky obsah?
	if($('#upload_step1 input[name=conditions]').is(':checked')) eroticContent = true;
	else eroticContent = false;

	//Pokud jsou vybrany soubory
	if(filesDataUp[0]) {
		//Souhlas s podminkama?
		if($('#upload_step1 input[name=conditions]').is(':checked')) {
			$("#upload_step1").hide();
			$("#upload_step2").show();
			allFilesSize();
			startUploading();
			
		} else {
			alert('Zapoměl/a jste zaškrtnout souhlas se všeobecnými podmínkami');
		}
		
	} else {
		alert('Vyberte prosim soubory, ktere si přejete nahrát na datové úložiště DATATOR.cz');
	}
}
function uploadCancel(){
	var cancel = confirm("Skutečně si přejete přerušit nahrávání?");
	if(cancel==true){
		window.location.reload(false);
	}
}
function uploadSpeed(){
	var speed = 0;
	if(allFirst == true){
		allFirst = false;
		allDoneKB = positionKB;
	}
	time++;
	var timeDate = secondsToTime(time);
	$("#L03").html(timeDate);
	speedKB.push(positionKB);
	if(speedKB.length>1){
		var last = speedKB.length - 1;
		speed = speedKB[last] - speedKB[last-1];
		
		if(speed) $("#L06").html(speed + " KB/s");
		else $("#L06").html("čekejte prosím");
		
		var remainingKB = totalKB - positionKB;
		var remainingTime = remainingKB / speed;
		var rTDate = secondsToTime(remainingTime);
		
		if(rTDate.trim() == "Invalid" || rTDate.trim() == "NaNvalid") $("#L04").html('čekejte prosím');
		else $("#L04").html(rTDate);
		
		allDoneKB += speed;
		var allRemaining = allFilesKB - allDoneKB;
		var allTime = allRemaining / speed;
		var allDate = secondsToTime(allTime);
		
		if(allDate.trim() == "Invalid" || allDate.trim() == "NaNvalid") $("#L05").html('čekejte prosím');
		else $("#L05").html(allDate);
	}	
}
function allFilesSize(){
	for(var q = 0;q<filesDataUp.length;q++){
		if(filesDataUp[q]==true){
			var kb = sizeToKB(filesData.item(q).size);
			var kbr = Math.round(kb);
			allFilesKB += kbr;
		}
	}
}
function secondsToTime(seconds)
{
    var t = new Date(1970,0,1);
    t.setSeconds(seconds);
    var s = t.toTimeString().substr(0,8);
    if(seconds > 86399)
    	s = Math.floor((t - Date.parse("1/1/70")) / 3600000) + s.substr(2);
    return s;
}
function newUp(){
	document.location.reload(false);
}
function copyUrl(element){
	$(element).prev('input').select();
}
function copyUrlInput(element){
	$(element).select();
}


function precise_round(value, decPlaces){
    var val = value * Math.pow(10, decPlaces);
    var fraction = (Math.round((val-parseInt(val))*10)/10);

    //this line is for consistency with .NET Decimal.Round behavior
    // -342.055 => -342.06
    if(fraction == -0.5) fraction = -0.6;

    val = Math.round(parseInt(val) + fraction) / Math.pow(10, decPlaces);
    return val;
}


//bytes to kB
function sizeToKB(size) {
	return precise_round(size / 1000, 2);
}

//bytes to MB
function sizeToMB(size) {
	return precise_round(size / 1000000, 2);
}

//bytes to GB
function sizeToGB(size) {
	return precise_round(size / 1000000000, 2);
}