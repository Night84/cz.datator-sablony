$( document ).ready(function() {
	
	$('.color_palete a').on('click', function(e) {
		
		e.preventDefault();
		
		var $sync = $(this).parent().data('sync');
		var $color = $(this).data('color');
		
		//nastaveni inputu
		$('#'+$sync).val($color);
		
		
		//nastaveni nahledu
		if($sync == 'color_link') {
		
			//obrazek
			$('.iframe_thumbs .thumb_box a.img img').css('border-color', '#'+$color);
			
			//odkaz
			$('.iframe_thumbs .thumb_box a.label').css('color', '#'+$color);
			
		}
		
	});
	
	
	//zmena barvy odkazu
	$('#color_link').on('keyup', function() {
		
		var $color = $(this).val();
		
		//obrazek
		$('.iframe_thumbs .thumb_box a.img img').css('border-color', '#'+$color);
		
		//odkaz
		$('.iframe_thumbs .thumb_box a.label').css('color', '#'+$color);
		
	});
	
	
	
	//Vygenerovani iframe
	$('#generate_iframe').on('click', function(e) {
		e.preventDefault();
		
		var $iframe_src = './iframe-nahledy.html';
		
		var $campaign = $('.form_files_thumbs #campaign').val();
		var $source = $('.form_files_thumbs #source').val();
		var $search = $('.form_files_thumbs #search').val();
		var $ordering = $('.form_files_thumbs #ordering').val();
		var $type = $('.form_files_thumbs #type').val();
		
		var $size_w = $('.form_files_thumbs #size_w').val();
		var $size_h = $('.form_files_thumbs #size_h').val();
		var $thumb_w = $('.form_files_thumbs #thumb_w').val();
		var $rows = $('.form_files_thumbs #rows').val();
		var $cols = $('.form_files_thumbs #cols').val();
		var $text_length = $('.form_files_thumbs #text_length').val();
		var $color_iframe = $('.form_files_thumbs #color_iframe').val();
		var $color_link = $('.form_files_thumbs #color_link').val();
		
		var $iframe = '<iframe src="'+ $iframe_src +'?campaign='+$campaign+'&source='+$source+'&search='+$search+'&ordering='+$ordering+'&type='+$type+'&thumb_w='+$thumb_w+'&rows='+$rows+'&cols='+$cols+'&text_length='+$text_length+'&color_iframe='+$color_iframe+'&color_link='+$color_link+'" style="border:none;width:'+$size_w+'px;height:'+$size_h+'px;overflow:hidden;"></iframe>';
		
		
		$('.form_files_thumbs #iframe_html').val($iframe);
		
		$('html, body').animate({
	        scrollTop: ($("#iframe_html").offset().top - 50)
	    }, 500);
		
	});
	
});