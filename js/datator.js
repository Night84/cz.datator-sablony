$( document ).ready(function() {
	
	//Vyhledavani - zobrazeni/skryti krizku
	$('.form_search input.search').keyup(function(e) {
		
		var $input_val = $(this).val();
		var $clear_input = $('.form_search .clear_search');
		
		
		if($input_val !== '') {
			$clear_input.show();
		
		} else {
			$clear_input.hide();
		}
		
	});
	
	//Vyhledavani - akce pri kliku nakrizek
	$('.form_search .clear_search').click(function(e) {
		
		e.preventDefault();
		
		$('.form_search input.search').val('').focus();
		$('.form_search .autocomplete').remove();
		$(this).hide();
		
	});
	
	
	
	
	//Prepinani tag a labels cloud na HP
	$('.hp_clouds .show_labels').click(function(e) {
		e.preventDefault();
		
		//odkazy
		$(this).addClass('active');
		$('.hp_clouds .show_tags').removeClass('active');
		
		//obsah
		$('.hp_clouds .tag_cloud_cover_hp').hide();
		$('.hp_clouds .labels_cloud_cover').show();
	});
	
	$('.hp_clouds .show_tags').click(function(e) {
		e.preventDefault();
		
		//odkazy
		$(this).addClass('active');
		$('.hp_clouds .show_labels').removeClass('active');
		
		//obsah
		$('.hp_clouds .labels_cloud_cover').hide();
		$('.hp_clouds .tag_cloud_cover_hp').show();
	});
	//Prepinani tag a labels cloud na HP
	
	
	
	//Prepinani jazyku na strance "nedostatek-kreditu.html"
	$('a.change_lang').click(function(e) {
		
		e.preventDefault();
		
		var $this = $(this);
		var $sms = '';
		var $tel = '';
		var $price = '';
		var $credit = '';
		
		if($this.hasClass('cz')) {
			if($this.data('sms-sk')) var $sms = $this.data('sms-sk');
			if($this.data('tel-sk')) var $tel = $this.data('tel-sk');
			
			if($this.data('price-sk')) var $price = $this.data('price-sk');
			if($this.data('credit-sk')) var $credit = $this.data('credit-sk');
			
			
			$this.removeClass('cz');
			$this.addClass('sk');
		
		} else {
			if($this.data('sms-cz')) var $sms = $this.data('sms-cz');
			if($this.data('tel-cz')) var $tel = $this.data('tel-cz');
			
			if($this.data('price-cz')) var $price = $this.data('price-cz');
			if($this.data('credit-cz')) var $credit = $this.data('credit-cz');
			
			$(this).removeClass('sk');
			$(this).addClass('cz');
		}
		
		
		if($('.change_sms').size()) $('.change_sms').html($sms);
		if($('.change_tel').size()) $('.change_tel').html($tel);
		if($('.change_price').size()) $('.change_price').html($price);
		if($('.change_credit').size()) $('.change_credit').html($credit);
		
	});


	//CHECKBOXES
	$('.checkbox_bg').click(function() {
		
		if($(this).hasClass('checked')) {
			$(this).removeClass('checked');
			$(this).find('input[type=checkbox]').prop('checked', false);
			
		} else {
			$(this).addClass('checked');
			$(this).find('input[type=checkbox]').prop('checked', true);
		}
	})
	//CHECKBOXES
	
	
	//SELECT BOXES
	if($('select.select_box').size()) {
		$('select.select_box').selectBoxIt();
	}
	//SELECT BOXES
	
	
	//Confirmation dialog
	$('a.confirm_dlg').click(function(e) {
        e.preventDefault();

        if (confirm( $(this).data('confirm') )) {
            $.get(this);
        }
    });
    //Confirmation dialog
    
    
    //DELETE ITEM BOX
    /*
    $('.item_box a.delete_box').click(function(e) {
	    e.preventDefault();
	    
	    $(this).closest('.item_box').fadeOut(function() {
		    
		    //pripadne volani ajaxu
		    
	    });
    })
    */
    //DELETE ITEM BOX
    
    
    //DELETE ITEM BOXES
    $('a.delete_queu').click(function(e) {
	    e.preventDefault();
	    
	    if (confirm( $(this).data('confirm') )) {
		    $('.my_section_results').find('.item_box').fadeOut(function() {
			    
			    //pripadne volani ajaxu
			    
		    });
	    }
    })
    //DELETE ITEM BOXES
    
    
    //SHOW INFO
    $('.show_info').hover(
    	function() {
	    	$(this).find('span').show();
    	}, 
    	function() {
	    	$(this).find('span').hide();
    	}
    );
    //SHOW INFO
    
    
    
    //THUMB RATING
    $('.rating a.thumbs').click(function(e) {
		e.preventDefault();
		
		$('.rating .thumbs').removeClass('active');
		$(this).addClass('active');
		
		if($(this).hasClass('up')) {
			
			//Nastavime hidden input na 1
			$('form input#thumb_rating').val(1);
			
			//Skryjeme duvod zaporneho hodnoceni
			$('#rating_reason').hide();
			
			//Zobrazime textarea + button na ulozeni
			$('.form_rating textarea, .form_rating input.button').show();
			
		} else if($(this).hasClass('down')) {
			
			$('form input#thumb_rating').val(2);
			$('#rating_reason').show();	
			
			//Skryjeme textarea
			$('.form_rating textarea').hide();
			
			//Zobrazime button na ulozeni
			$('.form_rating input.button').show();
		
		}
    });
    //THUMB RATING
    
    
    //RATING REASON
    $('#rating_reason select').on('change', function() {
	   
	   var $value = $(this).val();
	   
	   if($value > 0) {
	   		
	   		if($value == 1)  {
	   			$('#rating_reason .help_box').html('<p><strong class="red">TIP:</strong> <strong>NELZE SPUSTIT – stažené audio, video mi nejde spustit</strong></p><ul><li>Nainstalujte si prosím tyto video-kodeky, restartujte počítač a pokuste se prosím spustit soubor znovu: <a href="http://www.slunecnice.cz/sw/k-lite-mega-codec-pack/stahnout/" target="_blank">http://www.slunecnice.cz/sw/k-lite-mega-codec-pack/stahnout/</a></li></ul>');
		    
		    } else if($value == 2) {
			    $('#rating_reason .help_box').html('<p><strong class="red">TIP:</strong> <strong>NEJDE ZVUK, CHYBÍ DUBBING – videosoubor měl mít pravděpodobně český dubbing, ale nemá</strong></p><ul><li>soubor má pravděpodobně více zvukových stop (např. češtinu, angličtinu, španělštinu,…). Většinou stačí pouze přenastavit zvukovou stopu přímo v nastavení vašeho video-přehrávače. Nevíme, jaký přehrávač máte, ale <a href="http://www.zive.cz/poradna/kmplayer-nastaveni-zvukove-stopy/sc-20-cq-444258/default.aspx?consultanswers=1" target="_blank">zde</a> jistě najdete pomocnou ruku</li></ul>');
			    
		    } else if($value == 3) {
			    $('#rating_reason .help_box').html('<p><strong class="red">TIP:</strong> <strong>NIC SE NESTÁHLO - soubor se mi nepodařilo stáhnout</strong></p><ul><li>Není to možné, jinak by vám nepřišel tento email</li><li>Stiskněte pro jistotu klávesu CTRL + J, otevře se vám okno se staženými soubory. Jistě jej tam naleznete</li><li>Také máte možnost stejný soubor (po dobu několika hodin) stáhnout ZDARMA</li></ul>');
			    
		    } else if($value == 4) {
			    $('#rating_reason .help_box').html('<p><strong class="red">TIP:</strong> <strong>FAKE SOUBOR - soubor neodpovídá popisu</strong></p><ul><li>Stažený soubor se dle názvu tvářil jako něco jiného, než jste očekával/a?</li><li>Jméno souboru bylo např. tmavomodrý svět, ale po spuštění jste nalezl/a rodinné video? Potom máte pravdu a bude to tzv. fake soubor</li></ul>');
			    
		    } else if($value == 5) {
			    $('#rating_reason .help_box').html('<p class="no_margin"><strong class="red">TIP:</strong> <strong>Jiný problém</strong></p>');
			}
		    
		    $('#rating_reason .help_box').show();
		    
		    
		    if($value == 5 || $value == 4) {
			    //zobrazime textarea + button na ulozeni
			    $('.form_rating textarea').show();
			    
		    } else {
			    //Skryjeme textarea + button na ulozeni
				$('.form_rating textarea').hide();
		    }
		    
	   } else {
	   
	   		$('#rating_reason .help_box').html('');
		    $('#rating_reason .help_box').hide();
	   
	   }
	   
    });
    //RATING REASON
    
    
    //RATING FORM
    $('.form_rating').on('submit', function() {
		
		var $rating = $(this).find('input[name=thumb_rating]').val();
		var $reason = $(this).find('select[name=rating_reason]').val();
		var $text_length = $(this).find('textarea[name=rating_comment]').val().length;
		
		if($rating == 2 && $reason == 0) {
			
			alert('Vyberte prosím jednu z nabízených možností, abyste mohl/a hodnocení uložit.');
			return false;
			
		} else if($rating == 2 && ($reason == 5 || $reason == 4) && $text_length <= 0) {
		
			alert('Napište prosím důvod, proč hodnotíte soubor špatně. Je to bohužel povinné. ');
			return false;
		}
		    
    });
    //RATING FORM
    
    
    
    //Registration validation
    //Validate emailu
    $('.form_registration input.email').keyup(function() {
    	if(IsEmail($(this).val()) == true) $(this).parent().next('td').find('.result_icon').addClass('ok');
    	else $(this).parent().next('td').find('.result_icon').removeClass('ok');
    });
    
    
    //Validace hesla - min 3 znaky
    $('.form_registration input.password').keyup(function() {
    	//vynuluju fajfku u hesla2
    	$(this).parent().parent().next('tr').find('.result_icon').removeClass('ok');
    	
    	if($.trim($(this).val()).length > 3) $(this).parent().next('td').find('.result_icon').addClass('ok');
    	else $(this).parent().next('td').find('.result_icon').removeClass('ok');
    });
    
    
    //Validace heslo znovu - shoduje se s heslem 1
    $('.form_registration input.password2').keyup(function() {
    	if($(this).val() == $('.form_registration input.password').val()) $(this).parent().next('td').find('.result_icon').addClass('ok');
    	else $(this).parent().next('td').find('.result_icon').removeClass('ok');
    });
    //Registration validation
    
    
    
    //Date filters
    $('.date_filters .select_box').on('focus', function() {
	   
	   $('#date_type1').prop('checked', true);
	    
    });
    
    $('.date_filters #date_from, .date_filters #date_to').on('focus', function() {
	   
	   $('#date_type2').prop('checked', true);
	    
    });
    
    
    //Kalendar
    if($.datepicker) {
	    $.datepicker.regional['cs'] = {
	                closeText: 'Zavřít',
	                prevText: 'Předchozí',
	                nextText: 'Další',
	                currentText: 'Hoy',
	                monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen', 'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
	                monthNamesShort: ['Le','Ún','Bř','Du','Kv','Čn', 'Čc','Sr','Zá','Ří','Li','Pr'],
	                dayNames: ['Neděle','Pondělí','Úterý','Středa','Čtvrtek','Pátek','Sobota'],
	                dayNamesShort: ['Ne','Po','Út','St','Čt','Pá','So',],
	                dayNamesMin: ['Ne','Po','Út','St','Čt','Pá','So'],
	                weekHeader: 'Sm',
	                dateFormat: 'dd.mm.yy',
	                firstDay: 1,
	                isRTL: false,
	                showMonthAfterYear: false,
	                yearSuffix: ''};
	                
	    $.datepicker.setDefaults( $.datepicker.regional[ "cs" ] );
	    $('input.datepicker').datepicker({ dateFormat: "dd.mm.yy" });
    }
    
    
    
    //Close notice
    $('.close_notice').on('click', function(e) {
	   
	   e.preventDefault();
	   
	   $(this).parent().parent().slideUp(); 
	    
    });
    
    
    //Show all labels
    if($('.labels_cloud_cover.detail').size()) {
	    
	    $(this).find('.labels_cloud').each(function() {
			
			var $height = $(this).height();
			
			$(this).data('realHeight', $height);
			
			if($height <= 185) {
				$(this).find('.cloud_gradient').hide();
				$(this).parent().find('.more').hide();
			} 
			
		}).css({ overflow: "hidden", 'max-height': "180px" });
	    
    }
    
    $('.labels_cloud_cover.detail .more').on('click', function(e) {
	   
	   e.preventDefault();
	   
	   var $link = $(this);
	   var $labels = $link.parent().prev('.labels_cloud');
	   var $labels_height = $labels.height();
	   var $real_height = $labels.data('realHeight') - 5;
	   
	   if($real_height > $labels_height) { 
	   	   $link.fadeOut();
	   	   $labels.find('.cloud_gradient').fadeOut();
	   	   
	       $labels.animate({ 'max-height': $real_height }, 600); 
	   
	   }
	    
    });
    
    
    //Partner navigation
    $('.partner_navigation a').on('click', function(e) {
	   
	   e.preventDefault();
	   
	   var $target = $($(this).attr('href'));
	   
	   $('html, body').animate({
				
	       scrollTop: $target.offset().top
		
	   }, 700);
	   
    });
    
    
    //Copy URL General
    $('textarea.copy_text').click(function() {
		$(this).select();    
    });
	//Copy URL
	
	
	//Close wheel promo detail
	$('.wheel_promo_detail-close').on('click', function(e) {
    	
    	e.preventDefault();
    	
    	$('.wheel_promo_detail').hide();
    	$('#detail_right .share_box.wheel_promo').css('margin-top', 0);
    	
	});
	
	
	
	//open textarea with link
	$('a.share_link').click(function(e) {
    	
    	e.preventDefault();
    	
    	$('#copy_link_box').slideToggle();
    	
	});
    
});



function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
	   return false;
	}else{
	   return true;
	}
}