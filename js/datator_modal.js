$( document ).ready(function() {
	
	//otevreni modalu
	showModal('modal1');
	
	$('.modal_close').click(function(e) {
		e.preventDefault();
		
		$(this).closest('.modal').fadeOut();
	});
	
});


function showModal($modal_id) {

	$('#'+$modal_id).height($(document).outerHeight());
	$('#'+$modal_id).fadeIn();
		
}