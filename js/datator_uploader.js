$( document ).ready(function() {
	
	//Close info box
	$('a.close_upload_info_box').click(function(e) {
		e.preventDefault();
		
		$(this).closest('.upload_info_box').fadeOut();
	})
	//Close info box
	
	
	//Cancel upload
	$('a.cancel_upload').click(function(e) {
		e.preventDefault();
		
		uploadCancel();
	});
	//Cancel upload
	
	//Show upload details
	$('a.show_upload_details').click(function(e) {
		e.preventDefault();
		
		$('.upload_details').slideToggle(function() {
			
			if($(this).css('display') == 'block') {
				$('a.show_upload_details').text('Skrýt detaily');
				
			} else {
				$('a.show_upload_details').text('Zobrazit detaily');
			}
			
		});
	})
	//Show upload details
});