$( document ).ready(function() {

	//Obarveni radku
	$('.partner_stats tr.stats:odd').addClass('bg');
	
	//Zobrazeni/skryti grafu
	$('.partner_stats tr.stats, .show_graph').on('click', function(e) {
		
		e.preventDefault();
		
		var $tr_graph = $(this).next('tr.graph');
		
		if($tr_graph.size()) {
			
			//otevirani
			if($tr_graph.css('display') == 'none') {
				
				$tr_graph.show(0, function() {
					
					$tr_graph.find('.graph_cover').slideDown(500);
					drawCharts();
					
				});
			
			//zavirani	
			} else {
			
				$tr_graph.find('.graph_cover').slideUp(500, function() {
					
					$tr_graph.hide(0);
					
				});
			}
			
		}
		
	});
	
	
	//zavreni grafu
	$('a.graph_close').on('click', function(e) {
		
		e.preventDefault();
		
		var $tr_graph = $(this).parent().parent().parent();
		
		$tr_graph.find('.graph_cover').slideUp(500, function() {
					
			$tr_graph.hide(0);
			
		});
		
	});
	
});


//Vykresleni grafu
google.load("visualization", "1", {packages:["corechart"]});