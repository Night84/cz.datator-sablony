$( document ).ready(function() {
	
	$('.item_box .img img').hover(
	
		function() {
		
			var $img = $(this);
			
			if($img.data('thumbs')) {
			
				var $images = $img.data('thumbs').split("#");
				var $index = 0;
				
				window.$timer = setInterval(function() {
					
					if ($index == ($images.length - 1)) {
						$index = 0;
					    
					} else {
					 	$index++;
					}
					    
					$img.attr('src', $images[$index]);	
					
				}, 700);
			
			}
			
		}, 
		
		function() {
		
			clearInterval(window.$timer);
			
			var $img = $(this);
			
			if($img.data('thumbs')) {
			
				var $images = $img.data('thumbs').split("#");
				
				$img.attr('src', $images[0]);
			
			}
			
		}
		
	);
    
});